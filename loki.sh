#!/bin/bash
USERNAME=dthoren
HOSTS="loki.ist.unomaha.edu"
SCRIPT="cd public_html;cd group8-david-thoren;ls;git pull; publish-public-html"
for HOSTNAME in ${HOSTS} ; do
    ssh -l ${USERNAME} ${HOSTNAME} "${SCRIPT}"
done